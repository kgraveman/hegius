def outer():
#     print("no obst:")
#     print(globalDict["no_obstacle"])
#     print("just turned:")
#     print(globalDict["just_turned"])
#     if (globalDict["just_turned"]) and globalDict["no_obstacle"] == True:
#         print("yaaaaaaaa")
    #####################
    # Door script 1.0 Copyright Kevin Graveman
    # Requires a modified version MouseLook.py. (see appendix on line TODO xxx) The original version can be found at
    # www.tutorialsforblender3d.com
    # This script was made possible thanks to information and support from, among others, 
    #    MouseLook.py, a friend, my girlfriend, blenderartists.org, Social, and coursera.org
    # -------------------------------
    # Door orientation: 
    #   Door orientation does not matter, apply rotation to set initial position.
    #   After applying rotation you can rotate the door to set it to a desired state (e.g. half-open or open).
    #   
    #   
    #   Empty's orientation should be correct and adjustment should not be necessary.
    #   (Empty's X should point outward, empty's Y should point towards push open direction.)
    # 
    #   DO NOT FORGET TO APPLY ROTATION AFTER PLACING DOOR (do not worry about the emptys rotation, leave it as is)
    #   IF CW doors mouse operation seems flipped, check combMove function signs, if that doesnt work play with the empty's orientation
    #
    #####################   TODO
    # Todo: optimization:
    # see http://stackoverflow.com/questions/725782/in-python-what-is-the-difference-between-append-and for timing script speed
    #
    #
    ############ TODO
    # Some of the other things to do to improve this script:
    #   Remove things not being used
    #   add door momentum (using time, mass, velocity? velocity can only be calculated with delta time?)
    #   add sound effects
    #   add door states that help NPC's interact with the door
    #   read, add and fix comments
    #   make file paths relative
    #   make it so that sound stops when combinedMove is below a certain value
    #   edit soundfiles
    #   close door volume according to move delta
    #   about avoiding global dictionaries http://blenderartists.org/forum/archive/index.php/t-185496.html (store in properties?)
    #   combine limitMtxTup and doorLimits?
    #   doorMovingDelta() can be calculated with combinedmove, i think
    #
    #    This module is currently loaded by FPSCamera, not FPSController, check 
    #    if this is a problem.
    ########## BUGS
    # bugs:
    #       Sound
    #   
    #   door open click plays if closing door if E was released and door was in open or middle state
    #   door state 3 is not reached if door is shut too fast or hard
    #   door cannot be slammed shut repeatedly
    #   door middle audio repeats if e is released while moving door (best solution: use momentum to keep the door moving)
    #   
    #
    #       
    #       Door movement
    #
    #   CW doors when placed and rotationapplied mouse input is reversed! (for 90 degrees Z)
    
    ######## alternative solutions (denoted by ALT)
    #   replace scene with parent/child referencing
    #
    #
    #
    #
    #
    #######
    # define main program
    # logic.getCurrentController()
    
    def main():                     # only assignments and function calls in main?
        #             #           #           #      #   # #  ##Number sign for things that require attention:
        
        # set default values
        interactDistance = 5    # depends on game measurement settings
        
        
        # get controller
        cont = logic.getCurrentController()
        
        # get the object this script is attached to
        own = cont.owner
        
        # get the global dictionary for inter script communication
        gDict = logic.globalDict
        
        # get the current scene
        scene = logic.getCurrentScene()
    
    
        # set flags to prevent KeyErrors and such                       # probably can avoid many of these
        setFlags()
        
        # get the object under crosshairs      
        getRayedObject()
        
        # the below require rObj to be set, if it is not, might just as well skip to end?
        try:                                                                           ##### any way to make this prettier?
            gDict["rObj"]
        except KeyError:
            debugList.append("rObj was not defined")
        else:
            if gDict["rObj"] != None:
                
                # get mouse movement
                mMove = mouseMove(scene)
                
                # limit positions in matrices CLOSED        OPEN CCW                OPEN CW
                limitMtxTup = ([[1,0,0],[0,1,0],[0,0,1]],[[0,-1,0],[1,0,0],[0,0,1]],[[0,1,0],[-1,0,0],[0,0,1]])
                
                # get door type #0 == ccw 1 == cw
                type = doorType()
                
                # get door limits per type
                limits = doorLimits(type)
                
                # get door info (i.e. where the door handle is to which virtual force is applied)
                doorInfo = getDoorInfo(own)
                
                # get the distance from player to door
                rangeToDoor(doorInfo, interactDistance)
                
                # prevent AttributeError when moving away from the door while interacting
                #   or if rObj target is lost otherwise
                if (gDict["rObj"] != None):
                    
                    # get the combined mouse movement, converted to rotation to be applied
                    combinedMove = combMove(doorInfo, mMove, type)
                    
                    # get the intended rotation
                    intendRot = getIntendRot(type, combinedMove)
                    
                    # check if the intended rotation is legal
                    legalMove = preCheckLimit(limits, intendRot)
                    
                    # get the current door state (i.e. (1)a closed door, (2)in between, (3)closing shut.
                    doorState = getDoorState(limitMtxTup, combinedMove, type)
                    
                    # door position prod A for door delta
                    doorMovingA()
                    
                    # apply door rotation
                    applyRotation(legalMove, combinedMove, limitMtxTup, gDict, type)
                    
                    # door position prod B for door delta
                    doorMovingB()
                    
                    # get the difference position because of the (currently) applied rotation
                    doorMovingDelta()
                    
                    # load the required audio files
                    audioFile = loadFile(doorState)
                    
                    # create audio factories
                    createFactory(audioFile, doorState)
                    
                    # play the factories
                    playSound(doorState, combinedMove, type)
                    
                    # prevent middle from playing when door is not moving
                    checkMiddle()
                    
                    # debug logging
                    debug(combinedMove, doorState, doorInfo)
    
    #########################################################################################    
    
    # define set flags
    def setFlags():
        # debug flags
        global debugList 
        debugList = []
    
        
        ##global dict flags                                                     ###### really need these globals?             
        gDict["interactDoorFlag"] = False
        
        try: 
            gDict["factories"]
        except KeyError:
            gDict["factories"] = []
        else:                                               # is this even required?!
            gDict["factories"] = []
            
        try:
            gDict["isPlaying"]
        except KeyError:
            gDict["isPlaying"] = False
            
        try:
            gDict["closeShut"]
        except KeyError:
            gDict["closeShut"] = None
            
            
        try:
            gDict["handleOpen"]
        except KeyError:
            gDict["handleOpen"] = None
        try:
            gDict["handleMiddle"] 
        except KeyError:
            gDict["handleMiddle"] = None
        try:
            gDict["handleClose"] 
        except KeyError:
            gDict["handleClose"] = None
        try:
            gDict["handleHard"]
        except KeyError:
            gDict["handleHard"] = None
            
    #########################################################################################        
            
    def getRayedObject():#, gDict):                 # can i get rid of tempObj?
        ray = cont.sensors["Ray"]
        if (ray.hitObject != None):
            tempObj = ray.hitObject
            if "actDoor" in tempObj.name:
                gDict["rObj"] = tempObj
                #print("robj becomes tempobj which was: ", tempObj)
    
    # returns where the door is (obsolete?) and from which point the door is interacted with (handle)(empty)
    def getDoorInfo(own):#, gDict):
        if ("actDoor" in gDict["rObj"].name): #and (gDict["rObj"] != None) #forgot to put .name
    
            for child in gDict["rObj"].children:
                if "Empty" in child.name:
                    #link empty object to variable
                    rEmpty = child
                   
                    doorLoc = gDict["rObj"].getVectTo(own)                  #what are we using this for?????
                    doorHandle = rEmpty.getVectTo(own)
                    return (doorLoc, doorHandle)              #return not required if using gDict? Change if not using gDict, also change the caller
    
                else:
                    print("Error: Did not find empty")
        
            #block was here
            
        else:
            print("not an actDoor")
        
    # check if the door turns CW or CCW when opening
    def doorType():#gDict):                                                
        #already checked if looking at a door, assume a door has either C-C or something else (i.e. just C) in its name
        try:
            gDict["rObj"].name
        except AttributeError:
            print("rObj not set or flushed")
        else:
            if "C-C" in gDict["rObj"].name:  #forgot to put .name
                #print("CCW door found")
                return 0                      # 0 == counter-clockwise
            elif "C" not in gDict["rObj"].name:                                                  #DEBUG LINE
                print("error no C found in door object")                                    #DEBUG LINE (anything with print is isnt it)
                print("rayedobject was: ", gDict["rObj"])
            else:
                #0print("CW door found")
                return 1                      # 1 == clockwise
                
    
    # check if within range AND IF SO DISABLE MOUSELOOK.PY ?            #might want to only read range here and compare in final function (i.e. applyrotation)
    def rangeToDoor(doorInfo, interactDistance):#, gDict):
        if doorInfo != None:
            if doorInfo[1][0] < interactDistance:
                #disable MouseLook
                gDict["interactDoorFlag"] = True                                    #ugly name?
            else:
                gDict["rObj"] = None
                #print("rangetodoor resets robj")
        else:
            print("TESTerror: doorinfo not set")    
    
    # return door limits for given type
    def doorLimits(type):
        if type == 0:    #CCW
        #           Xx Xx  Yx Yx Xy Xy YY YY    (2x2 matrix, because rotating around Z) (first value is always lowest number i.e. do NOT take positions (matrix open vs matrix closed) into account)
        #           0  1   2  3  4  5  6  7    
            return (0, 1, -1, 0, 0, 1, 0, 1) #90 degrees CCW
        elif type == 1:  #CW
            return (0, 1, 0, 1, -1, 0, 0, 1) #90 degrees CW
    
    # mouseMove ########### From MouseLook.py
    def mouseMove(scene):
        mouse = scene.objects["FPSController"].sensors["MouseLook"]                 #ALT: use child/parent
        gameScreen = (bge.render.getWindowWidth(), bge.render.getWindowHeight())
        width = gameScreen[0]
        height = gameScreen[1]
        x = width/2 - mouse.position[0]
        y = height/2 - mouse.position[1]
        if not 'mouseInit' in scene.objects["FPSController"]:
            scene.objects["FPSController"]['mouseInit'] = True
            x = 0
            y = 0
        if not mouse.positive:
            x = 0
            y = 0
        return (x, y)
    
    # combine mouse movement and orientate them so the combined movement translates into radians
    def combMove(doorInfo, mMove, type):
        if len(gDict["rObj"].children) == 1:
            if type == 0:
                #               handle, local, 1=x 2=y
                yVect = -doorInfo[1][2][1]
                xVect = -doorInfo[1][2][0]                                                  ##flipping this SO DONT FLIP MOUSE?
            elif type == 1:
                yVect = -doorInfo[1][2][1]          #CHECK THE SIGNS HERE FIRST WHEN MOUSE SEEMS FLIPPED
                xVect = doorInfo[1][2][0]
            else:
                print("checkMove type error, door is neither CW nor CCW?!")
            #multiply vector Y axis[2][1] with mouse Y input 
            # ... and apply scale down to that (e.g. tanh data /900)    
            yMove = tanh((yVect * mMove[1])/900)
            xMove = tanh((xVect * mMove[0])/900)
        
                                                            #deadzone: (X <= -0.1) or (X >= 0.1) #########implement a deadzone for Y too!
            
        
            combinedMove = yMove + xMove                                        ####### CHECK THIS FIRST IF Mx and My MOVEMENT SEEMS TO BE CANCELLING EACH OTHER OUT
            return combinedMove
        else:
            print("TEST-error object children != 1")
    
    ######### Matrix tools, learned from Social####################
    #supply rObj.localOrientation and moveY
    def mtxMult(mtxOri, radApply):
        #if doorType[1] == "push":               # instead of these, just get combinedMove? # i think push/pull is automatically taken into account for because i multiply with the vector now
        #    radApply = tanh(mouseMY/900)
        #elif doorType[1] == "pull":
        #    radApply = -tanh(mouseMY/900)                   #i forgot about this, this is required because from the other side of the door rotation is the other way aroun too
        mtxApply = [
        		  [cos(radApply),-sin(radApply), 0],
        		  [sin(radApply), cos(radApply), 0],
        		  [0       , 0       , 1]]
        mtxNew = [[0,0,0],
    	  		[0,0,0],
    	  		[0,0,1]]
        for i in range(2):
            for j in range(2):
                a = mtxOri[i][0] * mtxApply[0][j]
                b = mtxOri[i][1] * mtxApply[1][j]
                mtxNew[i][j] = a + b
        return mtxNew
                                                        
    
    # calculate where the door will end up if rotation is applied (without limiting)                    #ALSO ADD A FUNCTION OR SOMETHING SO THAT DOOR CAN ONLY BE MOVED IN ONE (correct) DIRECTION WHEN -ON- THE LIMIT (instead of resetting it each time it goes over)
    def getIntendRot(type, combinedMove):#, gDict):          #instead of mMove get combinedMove
        if type == 0:       #ccw
            intendRot = mtxMult(gDict["rObj"].localOrientation, combinedMove)  #probably have to flip sign for CW door? 
            return intendRot
        # CHECK SIGNS:
        elif type == 1:     #cw
            intendRot = mtxMult(gDict["rObj"].localOrientation, combinedMove) #shouldnt these also get tanh and /500? NO! done in mtxMult function. #### CHECK SIGNS #had a minus in front of combinedMove, removed
            return intendRot
    
    # check if door will be within limits after rotation is applied
    def preCheckLimit(limits, intendRot):
        if (limits[0] <= intendRot[0][0] <= limits[1]) and (
            limits[2] <= intendRot[0][1] <= limits[3]) and (
            limits[4] <= intendRot[1][0] <= limits[5]) and (
            limits[6] <= intendRot[1][1] <= limits[7]):
            return 1
        else:
            return 0
    
    def doorMovingA():
        gDict["doorDeltaA"] = gDict["rObj"].localOrientation[0][0]
    
    # rewrite and apply functional programming to the function below    
    # apply rotation #ALSO ADD A FUNCTION OR SOMETHING SO THAT DOOR CAN ONLY BE MOVED IN ONE (correct) DIRECTION WHEN -ON- THE LIMIT (instead of resetting it each time it goes over)(sorry for shouting)
    def applyRotation(legalMove, combinedMove, limitMtxTup, gDict, type):
        #if legal move, apply it!
        # but only apply towards one direction if on a limit!
        if legalMove == 1:
            # middle
            if gDict["rObj"].localOrientation not in limitMtxTup: #current position# == #one of the limits# .. iterate? nah.. there are only 3 limits in total"
                gDict["rObj"].applyRotation((0, 0, combinedMove), True)
                return 2
            else:   # redundant else
                #print("HEREHERE")
                # door is closed, proceed to open
                if gDict["rObj"].localOrientation == limitMtxTup[0]:
                    if combinedMove > 0:                                        # not sure if this should be > or <
                        print("moving only in the direction your currently moving the mouse in")                                            
                        gDict["rObj"].applyRotation((0, 0, combinedMove), True)   #### not sure if should check for door type.. no.. combinedMove should already take type into account
                        return 1
                # door is open ccw, proceed to close
                elif gDict["rObj"].localOrientation == limitMtxTup[1]:
                        if combinedMove < 0:                                        # not sure if this should be > or <
                            print("moving only in the direction your currently moving the mouse in")                                            
                            gDict["rObj"].applyRotation((0, 0, combinedMove), True)
                            return 3
                # door is open cw, proceed to close
                elif gDict["rObj"].localOrientation == limitMtxTup[2]:
                        if combinedMove < 0:                                        # not sure if this should be > or <
                            print("moving only in the direction your currently moving the mouse in")                                            
                            gDict["rObj"].applyRotation((0, 0, combinedMove), True)
                            return 3
                        
                else:
                    print("applyrotation error")                            ## if this never occurs you could remove this and combine the elifs
        #if not a legal move, set to a limit! (sets to the closest limit, assuming mouse movement cant be THAT extreme)
        elif legalMove == 0:
            #print(gDict["rObj"].localOrientation[0])
            #print("type is :", type)
            if gDict["rObj"].localOrientation[0][0] < 0.3:
                if type == 0:
                    gDict["rObj"].localOrientation = limitMtxTup[1]#open door CCW
                    #print("opening door")
    ######new idea block#### seehttp://stackoverflow.com/questions/2785821/is-there-an-easy-way-in-python-to-wait-until-certain-condition-is-true           
    #                import time
    #                def waituntil((gDict["rObj"].localOrientation == limitMtxTup[1]), timeout, period=0.25):
    #                    mustend = time.time() + timeout
    #                    while time.time() < mustend:
    #                        if gDict["rObj"].localOrientation == limitMtxTup[1](): return True
    #                        time.sleep(period)
    #                    return False
    ######new idea block end####                                
                elif type == 1:
                    gDict["rObj"].localOrientation = limitMtxTup[2] #open door CW
            elif gDict["rObj"].localOrientation[0][0] > 0.7:
                if type == 0:
                    gDict["rObj"].localOrientation = limitMtxTup[0] #close door
                elif type == 1:
                    gDict["rObj"].localOrientation = limitMtxTup[0]        
            else:
                print("I'm not moving.. you're asking too much of me!")             ################ this doesnt cut it... door needs to be set to max or min ... and perhaps even bounce back a bit if this happens
        else:
            print("ERROR! legal move not defined!")
    
    # check if door is moving part B                            
    def doorMovingB():
        gDict["doorDeltaB"] = gDict["rObj"].localOrientation[0][0]
    
    # check if door is moving part C    
    def doorMovingDelta():
        gDict["doorDelta"] = gDict["doorDeltaA"] - gDict["doorDeltaB"]
    
    
    # returns whether the door is considered "closed", "inBetween", or "fullyOpen"
    def getDoorState(limitMtxTup, combinedMove, type):
        #print(type, gDict["rObj"].localOrientation[0][0], combinedMove)
        #print("combmove: ",combinedMove)
        # a closed door
        if gDict["rObj"].localOrientation[0][0] > 0.995:                              #door open threshold
            try:
                gDict["doorHandleUsed"]
            except KeyError:
                gDict["doorHandleUsed"] = False
            else:
                if gDict["doorHandleUsed"] == False:
                    
                    return 1
        # an open door closing shut         #combinedmove closer to zero is more sensitive #local orientation determines activation range
        if (type == 0) and (combinedMove < -0.05) and (gDict["rObj"].localOrientation[0][0] > 0.98):  # combinedMove for CCW is negative (for CW positive) and door ends up on [0][0] 1 then
    
            return 3
        elif (type == 1) and (combinedMove > 0.05) and (gDict["rObj"].localOrientation[0][0] > 0.98):    # can i combine this with the above?
            return 3
        # everything in between
        elif (combinedMove > 0.01) or (combinedMove < -0.01):
            
            return 2
    
        else:
            return 0
        
    
    
    
    ################### Sounds ###################################################
    # loads a sound file to be played by playSound, depending on the state the door is in (i.e. opening, moving, closing, closing hard)
    def loadFile(doorState):
    
        # on opening
        if doorState == 1:
    
            return "F:/blenderprojects/ahegius/audio/freesound/doors/handle/151577__d-w__door-handle-open-01.wav"
        # on middle, over threshold speed from combined mouse movement
        elif doorState == 2:
            return "F:/blenderprojects/ahegius/audio/freesound/doors/opening/148114__zabuhailo__doorcreaking9.wav"
        # on closing
        elif doorState == 3:
    
            return "F:/blenderprojects/ahegius/audio/freesound/doors/shut/117614__soundmary__door-close.wav"
        # on close hard
        ###### just use volume with delta? could also add a separate file for fun
        
        # opening gently
        elif doorState == 0:
            return 0
        
    # create a factory object using audiofile and doorstate
    def createFactory(audioFile, doorState):
        try:
            doorState
        except NameError:
            doorState = None
        else:
            import aud
            if doorState == 0:
                return None
            elif doorState == 1:
                gDict["factOpen"] = aud.Factory(audioFile)
            elif doorState == 2:
                gDict["factMiddle"] = aud.Factory(audioFile)
            elif doorState == 3:
                gDict["factClose"] = aud.Factory(audioFile)
            elif doorState == 4:
                gDict["factHard"] = aud.Factory(audioFile)
            else:
                print("createFactory error")
    
    
    # plays sound until end, creates handles for each type, makes sure a sound is only played once at a time
    def playSound(doorState, combinedMove, type):
        import aud
        device = aud.device()
        
        if (doorState == 4) and ((gDict["handleHard"] == None) or (gDict["handleHard"].status == False)):
            gDict["handleHard"] = device.play(gDict["factHard"])      
        if (doorState == 3) and ((gDict["handleClose"] == None) or (gDict["handleClose"].status == False)) and (gDict["doorDelta"] != 0):
            # combine doorDelta with volume to play this sound according to how hard the door is closed
            #volume = gDict["doorDelta"] * 70
            volume = tanh(combinedMove**2 *150)
            print("playsound volume: ",volume)
            gDict["handleClose"] = device.play(gDict["factClose"].volume(volume))
        
        if (doorState == 1) and ((gDict["handleOpen"] == None) or (gDict["handleOpen"].status == False)) and (gDict["doorHandleUsed"] == False):
            if ((type == 0) and (combinedMove > 0)) or ((type == 1) and (combinedMove < 0)):
                gDict["handleOpen"] = device.play(gDict["factOpen"])                     #can we do this without gDict?
                gDict["doorHandleUsed"] = True
        if (doorState == 2) and ((gDict["handleMiddle"] == None) or (gDict["handleMiddle"].status == False)) and (gDict["doorDelta"] != 0):
            gDict["handleMiddle"] = device.play(gDict["factMiddle"])
    
    # checks if middle is playing while door is not moving, if it is, stops it
    def checkMiddle():                                   #maybe rename?
        try:
            if (gDict["doorDelta"] == 0) and (gDict["handleMiddle"].status != False):
                gDict["handleMiddle"].stop()
        except AttributeError:
            debugList.append("handleMiddle not a handle object yet ")
       
    def debug(combinedMove, doorState, doorInfo):        
        if (keyboard.events[bge.events.FKEY] == ACTIVE):
            print("Debug goes here")
            #print(gDict["rObj"])
            #print(combinedMove)
            #print(gDict["factories"])
            #print(gDict["doorHandleUsed"])
            print(gDict["handleMiddle"].status)
            #gDict["handleMiddle"].stop()
        if (keyboard.events[bge.events.RKEY] == ACTIVE):
            print(doorState)
            print(doorInfo[1][2][1])
    
    # reset (all) flags 
    def resetFlags():
        print("resetting flags")        #maybe not required when not using gDict?! or at least.. only have to reset gDict? (first put them in, then remove one by one)
        
    
    
    
    ######################################################################
    from bge import logic
    import bge
    keyboard = bge.logic.keyboard
    ACTIVE = bge.logic.KX_INPUT_ACTIVE
        
    if (keyboard.events[bge.events.EKEY] == ACTIVE):
        from bge import render
        from mathutils import Vector                                            ##Do i need these?
        from math import sin, cos, tan, atan, tanh, radians
        
        
        cont = bge.logic.getCurrentController()
        #ray = cont.sensors["Ray"]   
        gDict = bge.logic.globalDict
    
        main()
        
        
    else:
        gDict = bge.logic.globalDict
        #print("DONT FORGET TO DUMP FLAGS")
    #    try:                                    ## I dont think we need these trys anymore
    #        gDict["rObj"] = None
    #    except:
    #        gDict["rObj"] = None
        try:
            rEmpty = None
        except:
            rEmpty = None
        try:                                    #except for this one
            gDict["interactDoorFlag"] = False
        except:
            gDict["interactDoorFlag"] = False
        ##DUMP FLAGS##
        try:
            gDict["doorHandleUsed"]
        except KeyError:
            gDict["doorHandleUsed"] = False
        else:
            gDict["doorHandleUsed"] = False
    
                
                
                
                
                
                
    ############APPENDIX############
    ###############################################
    
    # Use the modified useMouseLook function below instead of the original in MouseLook.py
    
    ## define useMouseLook
    #def useMouseLook(controller, capped, move, invert, sensitivity):
    #				
    #	# get up/down movement
    #	if capped == True:
    #		upDown = 0
    #	else:
    #		upDown = move[1] * sensitivity * invert 
    ##edit
    #	#bge.logic.globalDict["xMouseMoveY2"] = upDown
    ##end of edit
    #		
    #	# get left/right movement
    #	leftRight = move[0] * sensitivity * invert 
    ##edit
    #	#bge.logic.globalDict["xMouseMoveX"] = leftRight
    ##end of edit
    #		
    #	# Get the actuators
    #	act_LeftRight = controller.actuators["LeftRight"]
    #	act_UpDown = controller.actuators["UpDown"]  
    #
    ##edit
    ############Disable mouse look: prevent drift on interaction##########################
    ##                                                                                   #
    #	try:
    #		if bge.logic.globalDict["interactDoorFlag"] == True:
    #			act_LeftRight.dRot = [ 0.0, 0.0, 0.0]        
    #			act_LeftRight.useLocalDRot = True            
    #	
    #			act_UpDown.dRot = [ 0.0, 0.0, 0.0]           
    #			act_UpDown.useLocalDRot = True               
    #	except KeyError:
    #		bge.logic.globalDict["interactDoorFlag"] == False
    ##                                                                                   #
    ######################################################################################
    #
    #	#if bge.logic.globalDict["interactDoorFlag"] == True:
    ## turn off the actuators 
    #	controller.deactivate(act_LeftRight)
    #	controller.deactivate(act_UpDown) 
    #
    #
    ##edit ### Disable mouse look: cut off actuator#######################
    ##                                                                   #
    #	try:
    #		if bge.logic.globalDict["interactDoorFlag"] != True:	
    #	# set the values  #(5 lines below including white is original)
    #			act_LeftRight.dRot = [ 0.0, 0.0, leftRight]      #from original
    #			act_LeftRight.useLocalDRot = True                #from original
    #	
    #			act_UpDown.dRot = [ upDown, 0.0, 0.0]            #from original
    #			act_UpDown.useLocalDRot = True                   #from original
    #	except KeyError:
    #		bge.logic.globalDict["interactDoorFlag"] == False
    ##                                                                   #
    ######################################################################
    #
    #	# Use the actuators 
    #	controller.activate(act_LeftRight)
    #	controller.activate(act_UpDown)
#outer() # I guess this is not required because the module call initiates the function?