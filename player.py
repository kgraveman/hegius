#===============================================================================
# player.py
# Blender v2.68a
#
# Basic player script.
# Actions include: 
# TODO
# Death
# Stairwalk  
#
# Author: Kevin Graveman
# 
# Copyright 2013 Kevin Graveman
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#===============================================================================


from bge.logic import globalDict
from random import choice
from bge import logic
from bge import events
import fnmatch

# Define main function
def main():
    
    cont = logic.getCurrentController()
    scene = logic.getCurrentScene()
    player_obj = scene.objects["FPSController"]
    

    show_hp(player_obj)

#===============================================================================
# End of main
#===============================================================================

def show_hp(player_obj):
    
    # Display current HP.
    try:
        HUDscene = [scene for scene in logic.getSceneList(
                                      ) if scene.name == "overlayHUD"][0]
        HUDscene.objects['HP_text']["Text"] = "HP: " + str(
                                                       player_obj['health'])
    except IndexError:
        pass        

def die():
    """Show game over screen on 0 HP.
    
    Menu screen with modified options.
    """

    
def stairwalk():
    """Allow the player to walk up and down stairs."""
    
    # Idea: Look for object name AND property
    # Problem: Cannot walk on top of other small objects
    # Thats ok, require to jump on top of them?
    # TODO Alternate method for climbing.
    # Conclusion: use initial idea.
    # Just property should be enough...
    
    # Case up stairs:
    # Near sensor OR search for closest stair object (using intervals)
    # Not sure which is the better method, trying near sensor for now.
    # Reason: player object can probably use the near sensor for multiple
    # purposes in future methods.
    
    
    
    
    # Case down stairs:

#===============================================================================
# End of stairwalk
#===============================================================================

#===============================================================================
# End of file player.py
#===============================================================================