#===============================================================================
# enemy.py
# Blender v2.68a
#
# Basic enemy behavior.
# Behavior includes: 
# moving toward player, avoiding basic obstacles, 
# going to the location where the player was last spotted.  
#
# Author: Kevin Graveman
# 
# Copyright 2013 Kevin Graveman
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#===============================================================================

#===============================================================================
# TODO
# Enemy getting stuck prevention.
# Conbine variables in dicts to clean up.
#
#===============================================================================

#===============================================================================
# Default physics notes
# Damping rotation 0.100

#===============================================================================


from bge.logic import globalDict
from random import choice
from bge import logic
from bge import events
import fnmatch



# Define main function
def main():

#===============================================================================
# set defaults
#===============================================================================

    cont = logic.getCurrentController()
    scene = logic.getCurrentScene()
    player_obj = scene.objects["FPSController"]
    #===========================================================================
    # testing block
#     print(cont.owner["timer_test"])
#     if cont.owner["timer_test"] > 0:
#         cont.owner["timer_test"] = 0
    
    # end of testing block
    #===========================================================================
    
    
    #TODO: Move variables to their respective functions.
#     print(type(player_obj.name)) #LEARNING
    
    # health
    
    # attack power
    
    #attack type (1,2,3)
    #e.g. grab, punch, bite, throw, tackle
    
    # move speed 1
    movespeed1 = 0.05 #0.05
    # move speed 2
    
    
    # move speed 3: strafing obstacle avoidance
    movespeed3 = 0.07
    # run speed 1
    
    #rotation speed general (placeholder)
    rot_spd1 = 0.05 #0.05
    
    #rotation speed while running
    
    #rotation speed while walking
    
    #rotation speed while standing in place
    
    # the distance to where the enemy approaches the player
    enemy_proximity = 1 # Default 1
    
    #how far the enemy can see
    sight_dist = 15 # Default 8
    
    # The angle in degrees which means 'facing'.
    facing_degrees = 0.5 # Really degrees?? # Default 0.5
    
    # The margin the 'facing rotation' has to be in for the enemy to move 
    # towards the player.
    facing_margin = 0.0001
    
    #the range of the obstacle avoidance rays
    ray_range = 2
    
    #######
    # variables
    ######
#     globalDict["just_turned"] = False
#     global just_turned
#     just_turned = False
    
    
#===============================================================================
# Run subfunctions
#===============================================================================

    if attack(cont, player_obj) != True:
        chase(cont, player_obj, enemy_proximity, facing_degrees, rot_spd1,
              facing_margin, movespeed1, ray_range, movespeed3, sight_dist)
    move(cont) #test method, rotates ccw on QKEY    
    
    
    
    #####################
    # define subfunctions
    #####################
    
    ##############
    #roll activity
    #randomly activate a non-attack (roam) activity
         
         
         
         
def chase(cont, player_obj, enemy_proximity, facing_degrees, rot_spd1, 
          facing_margin, movespeed1, ray_range, movespeed3, sight_dist):
    """Methods that describe chasing chasing behavior.
    
    Methods that are used to chase a player when spotted and when lost 
    out of sight after chasing.
    """
    # TODO IDEA: end chase and return to todo:'roaming' when 
    # 'enemy_lost_track/sight' animation reaches last 
    # frame (use getActionFrame)
    # TODO: Jump over obstacle.
#---------------------------------- case 1: direct line of sight, no obstacles.
    def path_straight(target_location=None):
#         print("straight path")
        """ Move in a straight line toward player."""
        # Determines if player is in LOS. (Supplied by 
        # path_obstacle_lost_sight.)
        if target_location == None: # Player visible
            # Find vector to player
#             print("debug 162")
            target_location = cont.owner.getVectTo(player_obj)
        else: # Player not visible
            target_location = cont.owner.getVectTo(target_location)

        # Apply rotation
        if (target_location[0] > enemy_proximity) and (
                       target_location[2][1] < 1-facing_degrees/90) and (
                                              target_location[2][0] < 0):
            # Note: from top positive rotation is ccw rotation.
            cont.owner.applyRotation([0, 0, rot_spd1], 1)                
        if target_location[0] > enemy_proximity and (
                         target_location[2][1] < 1-facing_degrees/90) and (
                                               target_location[2][0] > 0):
            # Note: so this is cw rotation.
            cont.owner.applyRotation([0,0,-rot_spd1],1)
    
        # Apply movement
        # Only apply movement if not yet within proximity.
        if target_location[0] > enemy_proximity:
            # Check if facing player, then apply movement.
            # BUG? If enemy shakes while trying to face the player, check 
            # this conditional:
            if target_location[2][1] > (1-facing_degrees/90-facing_margin):

                cont.owner.applyMovement([0, movespeed1, 0], 1)
    
#------------------------------------------------ Case 2: obstacle in move path.
    def path_obstacle_detector():
        """Uses ray sensors to check if there is an obstacle in the 
        movepath. Returns obst_right, obst_left, obst_both or False."""
        # NOTE: this is no substitute for (corridor) navigation waypoints.

        # Get the full names of the right and left origin rays (in case 
        # .n appendices are used).
        
        #===================================================================
        #---------------------------------------- merged into comprehension.
        # child_names = list()
        # for obj in cont.owner.children:
        #     child_names.append(obj.name)
        # del(obj)
        #===================================================================
        child_names = [obj.name for obj in cont.owner.children]
        
        # erxx stands for enemy ray right/left origin/target.
        erro_name = fnmatch.filter(
                               child_names, 'enemy_ray_right_origin*')[0]
        errt_name = cont.owner.children[erro_name].children[0].name
        erlo_name = fnmatch.filter(
                               child_names, 'enemy_ray_left_origin*')[0]
        erlt_name = cont.owner.children[erlo_name].children[0].name 
        del(child_names)
        
        # Cast a ray from the origin empties to their target empties,
        # and check for obstacles.
        # Check if BOTH rays are blocked.
        if cont.owner.children[erro_name].rayCastTo(
            cont.owner.children[erro_name].children[errt_name], 
            ray_range, 'obstacle') != None and cont.owner.children[
            erlo_name].rayCastTo(cont.owner.children[erlo_name].children[
            erlt_name], ray_range, 'obstacle') != None:
                cont.owner.children[erro_name].stopAction(7)
                cont.owner.children[erlo_name].stopAction(7)
                return 'obst_both'
        
        # check if the RIGHT ray is blocked by an obstacle
        elif cont.owner.children[erro_name].rayCastTo(cont.owner.children[
          erro_name].children[errt_name], ray_range, 'obstacle') != None:
            cont.owner.children[erro_name].stopAction(7)
            return 'obst_right'
        # check if the LEFT ray is blocked by an obstacle
        elif cont.owner.children[erlo_name].rayCastTo(cont.owner.children[
          erlo_name].children[erlt_name], ray_range, 'obstacle') != None:
            cont.owner.children[erlo_name].stopAction(7)
            return 'obst_left'
        
        # if ray is NOT blocked, sweep rays up and down to 
        # increase their detection window and return False: no obstacle
        else:
            cont.owner.children[erro_name].playAction(
                                              "erro_ray_sweep", 1, 20, 7)
            cont.owner.children[erlo_name].playAction(
                                              "erlo_ray_sweep", 1, 20, 7)
            return False
    
    def path_obstacle_avoid_right_sidestep():
        cont.owner.applyMovement([-movespeed3,0,0],1)
        
    def path_obstacle_avoid_left_sidestep():
        cont.owner.applyMovement([movespeed3,0,0],1)
        
#---------------------------------------------- case 3: LOS lost due to obstacle
    def path_obstacle_lost_sight():
        """Determines behavior when LOS is lost. 
        
        Calls a function depending on obstacles on the current
        path to the players last sighted position by the enemy sensor.
        """
    #:go to last known player position
    # LESSON "#:" comments are known as 'comment docstrings' and can be
    # used to comment a variable.
    #
    # Store last known player position and navigate to that spot as if 
    # the player is still there.
    # Lets cheat and get the player position right after it was lost.
    #
    # Use the same basic algoritm as when the player is visible.
    # Reinstate the normal algoritm if the player becomes visible again.
        try:
            globalDict["enemy_has_lost_path"]
        except KeyError:
            # TODO working on this
            # In case enemy has spawned (never seen the player) and is
            # blocked by obstacles:
            globalDict["enemy_has_lost_path"] = "spawned"
        
        if globalDict["enemy_has_lost_path"] == False:

        # LESSON: using copy in the code below was vital
        # as worldposition is not a static value, but a 'dynamic pointer'.
        # What is the correct terminology here? 
            
        # Store last known position until player is sighted again (set flag)
        # 'sighted again' is handled at the end of the chase method.
            globalDict[
               "enemy_last_player_pos"] = player_obj.worldPosition.copy()     
            globalDict["enemy_has_lost_path"] = True
        
        # Straight path to position available. 
        if path_obstacle_detector() == False:
            try:
                path_straight(globalDict["enemy_last_player_pos"])
            except KeyError:
                # Never seen player, pass this function and use
                # roaming or waypoint system. 
                # TODO check if we should use enemy_has_lost_path
                pass
        
        # Avoiding obstacles (in case of moving obstacles such as doors)
        # Right ray blocked by obstacle, avoid.
        elif path_obstacle_detector() == 'obst_right':
            path_obstacle_avoid_right_sidestep()

        # Left ray blocked by obstacle, avoid.
        elif path_obstacle_detector(ray_range) == 'obst_left':
            path_obstacle_avoid_left_sidestep()
            
        # Both rays blocked.
        elif path_obstacle_detector(ray_range) == 'obst_both':
            print("TODO both rays blocked, lost LOS")
            # TODO code movable or special obstacle specific responses
            # such as 'open door', 'find alternate path', or 'return
            # to waypoint'.

#------------------ Case 4: both rays are blocked (e.g. facing a wall or corner)
    # TODO NOTE: LOS is still possible in some cases!
    # For now, handle both cases the same way.
    def path_obstacle_both():
        """Obstacle avoidance when both rays are blocked.
        
        Rotates enemy randomly left or right until one sensor is clear
        of obstacles so path_obstacle_avoid_right/left_sidestep can resume
        chasing
        """
        # Randomly pick right or left, to avoid the old 'indecisive around 
        # the table' AI behavior.

        # Next rotate until one sensor is not blocked anymore.
        try:
            cont.owner.applyRotation([0,0,globalDict["lr_sign"]*rot_spd1],1)
        except KeyError:
            # TODO: remove this key after a certain amount of time.
            # (Use game property timer.)
            globalDict["lr_sign"] = choice([1, -1])
        
        # Note that players and otherwise movables should not be seen as 
        # obstacles. Finally, use the regular 
        # chase->path_obstacle_avoid_x method to proceed.
        #
        # TODO: give doors, players, and otherwise movables a property (use 
        # actor physics?) to determine how enemy interacts with it (e.g. 
        # open door). Handling of those events should be in 
        # path_obstacle_lost_sight.


    # Note: this is still part of the chase method.
#--------------------------------------------------- Case: Player visible by ray
    # Straight path to player available.
    if path_obstacle_detector() == False and cont.owner.rayCastTo(
                                      player_obj, sight_dist, 'player'):
        globalDict["enemy_has_lost_path"] = False
        path_straight()

    # Right ray blocked by obstacle, avoid.
    elif path_obstacle_detector() == 'obst_right' and cont.owner.rayCastTo(
                                       player_obj, sight_dist, 'player'):
        globalDict["enemy_has_lost_path"] = False
        path_obstacle_avoid_right_sidestep()

    # Left ray blocked by obstacle, avoid.
    elif path_obstacle_detector() == 'obst_left' and cont.owner.rayCastTo(
                                      player_obj, sight_dist, 'player'):
        globalDict["enemy_has_lost_path"] = False
        path_obstacle_avoid_left_sidestep()
        
    # Both rays blocked, avoid.
    # Note: compare this with path_obstacle_lost_sight's identical
    # statement. In this case line of sight was not lost.
    elif path_obstacle_detector() == 'obst_both' and cont.owner.rayCastTo(
                                      player_obj, sight_dist, 'player'): 
        globalDict["enemy_has_lost_path"] = False
        path_obstacle_both()
        
#------------------------------------------------------------------ Case: No LOS
    else:
        path_obstacle_lost_sight() # TODO should rename this to p_o_no_los.

#---------------------------------------------------------- End of chase method.

def attack(cont, player_obj):
    """Attack the player.
    
    When within range, play animation, at damage_frame subtract health from 
    player.
    Returns True on attack.
    """
    # TODO: Be sure to deactivate obstacle detection (otherwise e.g. 
    # obstacle avoidance rotation might kick in). Seems ok like this, 
    # but might want add a time margin around attack.
    damage = choice([15,16,17,18,19,20,21,22])
    
    # The idea behind damage_frame was to apply damage at a specific
    # ActionFrame. ActionFrames, however, seem to be represented in 
    # unpredictable float.
    # TODO: (optional) implement by looking for ActionFrame greater than, 
    # and once after action has started.
    # damage_frame = 15
    attack_range = 1.5
    attack_speed = 20 + 24 # Animation is 20 frames long
    animation_speed = 1
    
    #=======================================================================
    #-------------------------------------------- Merged into comprehension:
    # child_names = list()
    # [name for name in cont.owner.children] # comprehension
    # for obj in cont.owner.children:
    #     child_names.append(obj.name)
    # del(obj)
    #=======================================================================

    child_names = [obj.name for obj in cont.owner.children]

    claw_name = fnmatch.filter(child_names, 'claw*')[0]
    del(child_names)

    # Set the HUD to show player hitpoints. TODO: move this to player module
    # if/when you make one.
#     try:
#         HUDscene = [scene for scene in logic.getSceneList(
#                                       ) if scene.name == "overlayHUD"][0]
#         HUDscene.objects['HP_text']["Text"] = "HP: " + str(
#                                                        player_obj['health'])
#     except IndexError:
#         pass        

    # LESSON: cant rely on accuracy of measuring by ActionFrame
    # Alternative: Check if action is already playing.
    
    # Initial strike to prevent bugs...
    if (cont.owner.children[claw_name].getActionFrame(6) == 0):
        cont.owner.children[claw_name].playAction('claw_slash',
                                          1, attack_speed + 2, 6, 
                                  speed = animation_speed, play_mode = 0)
        global strike_done
        strike_done = True
    
    # Determines attack speed (how often enemy attacks).
    if cont.owner.children[claw_name].getActionFrame(6) > (
                           attack_speed) and strike_done == True:
        # LESSON: Sifting through bpy got me the animation name at last.
        # LESSON: ctrl-1 for undefinedvariable errors (PyDev).
        # LESSON: Do not use bpy in BGE. (context.scene.objects[
        #                      claw_name].animation_data.action.name)
        # Conclusion: Use (data) linked duplicates so they can all share the
        # same animation (animations will still only affect individual objects).
        
        # Attack if within range, and action is not playing, and 
        # player health is above 0. 
        if cont.owner.getDistanceTo(player_obj) < attack_range and (
                cont.owner.children[claw_name].isPlayingAction(
                               6) == False) and player_obj['health'] > 0:
            cont.owner.children[claw_name].playAction('claw_slash',
                                          1, attack_speed + 2, 6, 
                                  speed = animation_speed, play_mode = 0)
            # Deal damage to player.
            player_obj['health'] -= damage
            return True # To prevent chase().

#--------------------------------------------------------- End of attack method.

    #######
    #listen (requires player sound and ambient sound properties)
    #randomly activated (often) but more sensitive when actively seeking (hunting)
    
    #######
    #memory 
    #(check around an area where the player was last seen 
    #i.e. when the player leaves LOS a specialised search function takes over 
     
    #######
    #forget
    #after a random amount of time the specialised search function initiated by
    #memory terminates and another activity is rolled
    
    ######
    #prowl
    #conceil own sound emissions and move slower, 
    #if possible sticking to shadows
    #listen is increased
    
    ############
    #random hunt
    #for a (random) short duration, become active
    #increased speed and checking possible hiding spots (memory and preprogrammed)
    #however, listen ability will be lowered
    
#####
#hunt
#when player is spotted, this mode activates 
#and will last a random duration even after LOS is broken

########
#passive
#temporary sleep with all abilities decreased

########
#playful
#increased movement speed, does not directly engage
#flanks, limited duration

#######
# track

#####
# die
    
#############
#dodge attack
#when player aims at enemy, enemy will dodge after a random duration

###########
#plan ahead
#advances an empty object to the currently planned location
#empty can plan an action too and if that location is unfavorable, another
#initial action or move can be taken
#favored locations can differ, examples:
#open area, less illuminated area, closer to the player, different 
#location than starting position

###############
#go to waypoint
#to avoid enemies getting stuck in the same area all the time,
#AI attempts to reach a set number of waypoints for a given amount of actions taken
#after a set number of fails, a direct path to the waypoint will be taken
#if that fails, the enemy will teleport to a waypoint
#a list of recently visited waypoints is kept track of so the enemy
#will visit all locations while still being unpredictable
# Note that you can try to make doors into waypoints!  

##########
#animation
#instead of weak looping animations, try to animate characters using logic
# end of subfunctions

############
#personality
#e.g. yawn, scratch, anger, be startled, 


##########
#cooporate
#simple: slave to the enemy which is closest to the player
#master will remain master until all enemies involved are no longer
#in an active state
#treat master as an obstacle only when master is attacking to 
#enable slaves to move into attack position

#########
#multiplayer ideas
#pick player to lock on
#delegate target
#switch target
#flank
#call for backup

#===============================================================================
# Test methods
#===============================================================================

def move(cont):
    """Simple move test: turn CCW on QKEY."""
    # NOTE: chase method prevents this from working correctly.
#         print("test")
#         from bge import logic
    
#         import bge

#         print(cont)
#         ray = own.sensors["Ray"]
#         radar = cont.owner.sensors["Radar"]
    
#         if (radar.hitObject == player_obj):
#             print("hit hit")
#             cont.owner.applyMovement([0,movespeed1,0],1)

    keyboard = logic.keyboard
    ACTIVE = logic.KX_INPUT_ACTIVE
    if (keyboard.events[events.QKEY] == ACTIVE):
        print("Q KEY ACTIVE debug")
        cont.owner.applyRotation([0,0,0.02],1)

#------------------------------------------------------- End of simple move test

#===============================================================================
# End of test methods
#===============================================================================
        


    
#===============================================================================
# End of file enemy.py
#===============================================================================